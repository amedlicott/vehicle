package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.validation.constraints.*;

/*
JSON Representation for Response:
{
  "required": [
    "took",
    "total",
    "vehicle"
  ],
  "type": "object",
  "properties": {
    "vehicle": {
      "$ref": "Vehicle"
    },
    "total": {
      "description": "Total objects matching search (may be different than the actual number returned because of paging)",
      "type": "integer",
      "format": "int64"
    },
    "took": {
      "description": "Time taken for search",
      "type": "string"
    }
  }
}
*/

public class Response {

	@Size(max=1)
	@NotNull
	private Vehicle vehicle;

	@Size(max=1)
	@NotNull
	private Integer total;

	@Size(max=1)
	@NotNull
	private String took;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    vehicle = null;
	    
	    took = org.apache.commons.lang3.StringUtils.EMPTY;
	}
	public Vehicle getVehicle() {
		return vehicle;
	}
	
	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}
	public Integer getTotal() {
		return total;
	}
	
	public void setTotal(Integer total) {
		this.total = total;
	}
	public String getTook() {
		return took;
	}
	
	public void setTook(String took) {
		this.took = took;
	}
}